package tech.habegger.graphql.db;

import tech.habegger.graphql.db.meta.Relationship;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

public class Config {
    public Connection connection() throws SQLException {
        String url = "jdbc:postgresql://localhost/postgres";
        Properties props = new Properties();
        props.setProperty("user", "postgres");
        props.setProperty("password", "graphqlcourse");
        return DriverManager.getConnection(url, props);
    }
    public Map<String, Map<String, Relationship>> relationships() {
        Map<String, Map<String, Relationship>> relationships = new HashMap<>();
        relationships.put("city", Map.of(
            "popstats", new Relationship(
                "citypops",
                "country = ${string:country} AND province = ${string:province} AND city = ${string:name}",
                true
            ),
            "country", new Relationship("country", "code = ${string:country}",false),
            "province", new Relationship("province", "name = ${string:province} AND country = ${string:country}",false)
        ));

        relationships.put("country", Map.of(
            "capital", new Relationship("city", "name = ${string:capital} AND country = ${string:code}",false),
            "provinces", new Relationship("province", "country = ${string:code}",true)
        ));
        relationships.put("province", Map.of(
            "capital", new Relationship("city", "capital = ${string:name} AND country = ${string:country}",false),
            "country", new Relationship("country", "code = ${string:country}",true)
        ));

        return relationships;
    }

    public int port() {
        return 8080;
    }
}
