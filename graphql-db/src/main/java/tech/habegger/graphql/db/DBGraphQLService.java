package tech.habegger.graphql.db;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.language.FieldDefinition;
import graphql.language.ListType;
import graphql.language.ObjectTypeDefinition;
import graphql.language.Type;
import graphql.language.TypeName;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.habegger.graphql.db.fetchers.DBListRelationshipDataFetcher;
import tech.habegger.graphql.db.fetchers.DBSingletonRelationshipDataFetcher;
import tech.habegger.graphql.db.fetchers.DBTableDataFetcher;
import tech.habegger.graphql.db.meta.Relationship;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

public class DBGraphQLService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DBGraphQLService.class);
    private final GraphQL graphQL;

    public DBGraphQLService(Connection dbConnection, Map<String, Map<String,Relationship>> relationships) throws SQLException {
        GraphQLSchema schema = buildSchema(dbConnection, relationships);
        graphQL = GraphQL.newGraphQL(schema).build();
    }

    static GraphQLSchema buildSchema(Connection dbConnection, Map<String, Map<String,Relationship>> relationships) throws SQLException {
        var registry = new TypeDefinitionRegistry();
        var wiringBuilder = RuntimeWiring.newRuntimeWiring();

        var dbMeta = dbConnection.getMetaData();
        var tables = dbMeta.getTables(null,null,"%", new String[] {("TABLE")});

        var queryTypeBuilder = ObjectTypeDefinition.newObjectTypeDefinition().name("Query");

        // Create a type for each table and populate with the columns
        while(tables.next()) {
            var tableName = tables.getString("TABLE_NAME");
            var objectTypeBuilder = ObjectTypeDefinition.newObjectTypeDefinition()
                    .name(tableName);
            var tableRelationships = relationships.getOrDefault(tableName, Map.of());

            // Process columns
            var columns = dbMeta.getColumns(null, null, tableName, "%");
            while(columns.next()) {
                var fieldName = columns.getString("COLUMN_NAME");
                if(tableRelationships.containsKey(fieldName)) {
                    LOGGER.info("Skipping db field {}.{} as relationship is defined for it", tableName, fieldName);
                } else {
                    var fieldDefinition = FieldDefinition.newFieldDefinition()
                        .name(fieldName)
                        .type(new TypeName("String"))
                        .build();
                    objectTypeBuilder.fieldDefinition(fieldDefinition);
                }
            }

            // Process relationships
            for(var entry: tableRelationships.entrySet()) {
                var fieldName = entry.getKey();
                Type<?> fieldType = new TypeName(entry.getValue().target());
                if(entry.getValue().isList()) {
                   fieldType =  new ListType(fieldType);
                }
                var fieldDefinition = FieldDefinition.newFieldDefinition()
                    .name(fieldName)
                    .type(fieldType)
                    .build();
                objectTypeBuilder.fieldDefinition(fieldDefinition);
                if(entry.getValue().isList()) {
                    wiringBuilder.type(tableName, builder -> builder.dataFetcher(fieldName, new DBListRelationshipDataFetcher(entry.getValue(), dbConnection)));
                } else {
                    wiringBuilder.type(tableName, builder -> builder.dataFetcher(fieldName, new DBSingletonRelationshipDataFetcher(entry.getValue(), dbConnection)));
                }
            }

            // Define and wire table
            registry.add(objectTypeBuilder.build());
            var tableEntrypointField = FieldDefinition.newFieldDefinition()
                .name(tableName)
                .type(new ListType(new TypeName(tableName)))
                .build();
            queryTypeBuilder.fieldDefinition(tableEntrypointField);
            wiringBuilder.type("Query", builder -> builder.dataFetcher(tableName, new DBTableDataFetcher(tableName, dbConnection)));
        }
        registry.add(queryTypeBuilder.build());

        SchemaGenerator schemaGenerator = new SchemaGenerator();
        return schemaGenerator.makeExecutableSchema(registry, wiringBuilder.build());
    }

    public ExecutionResult execute(String query, Map<String, Object> variables) {
        ExecutionInput.Builder executionInput = ExecutionInput.newExecutionInput()
            .query(query);
        if(variables != null) {
            executionInput.variables(variables);
        }
        return graphQL.execute(query);
    }

}
