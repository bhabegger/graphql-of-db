package tech.habegger.graphql.db.fetchers;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Map;

public abstract class DBAbstractDataFetcher<T> implements DataFetcher<T> {

    private final Connection connection;

    public DBAbstractDataFetcher(Connection connection) {
        this.connection = connection;
    }
    @Override
    public T get(DataFetchingEnvironment environment) throws Exception {
        var sqlQuery = queryFor(environment.getSource());
        var statement = connection.prepareStatement(sqlQuery);
        var results = statement.executeQuery();
        int cols = results.getMetaData().getColumnCount();
        String[] names = new String[cols];
        for(int i=0; i<cols; i++) {
            names[i] = results.getMetaData().getColumnName(i+1);
        }

        return prepareResults(results, names);
    }

    abstract T prepareResults(ResultSet results, String[] names) throws SQLException;

    abstract String queryFor(Map<String, Object> source);
}
