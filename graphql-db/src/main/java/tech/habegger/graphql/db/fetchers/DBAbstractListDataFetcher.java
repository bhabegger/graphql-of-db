package tech.habegger.graphql.db.fetchers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public abstract class DBAbstractListDataFetcher extends DBAbstractDataFetcher<List<Map<String, Object>>> {

    public DBAbstractListDataFetcher(Connection connection) {
        super(connection);
    }

    ArrayList<Map<String, Object>> prepareResults(ResultSet results, String[] names) throws SQLException {
        var output = new ArrayList<Map<String, Object>>();
        while(results.next()) {
            var entry = new LinkedHashMap<String, Object>();
            for (int i = 0; i < names.length; i++) {
                entry.put(names[i], results.getString(i+1));
            }
            output.add(entry);
        }
        return output;
    }
}
