package tech.habegger.graphql.db.fetchers;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

public abstract class DBAbstractSingletonDataFetcher extends DBAbstractDataFetcher<Map<String, Object>> {

    public DBAbstractSingletonDataFetcher(Connection connection) {
        super(connection);
    }

    @Override
    Map<String, Object> prepareResults(ResultSet results, String[] names) throws SQLException {
        var entry = new LinkedHashMap<String, Object>();
        if(results.next()) {
            for (int i = 0; i < names.length; i++) {
                entry.put(names[i], results.getString(i+1));
            }
        }
        return entry;
    }

    abstract String queryFor(Map<String, Object> source);
}
