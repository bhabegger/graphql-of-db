package tech.habegger.graphql.db.fetchers;

import org.apache.commons.text.StringSubstitutor;
import tech.habegger.graphql.db.meta.Relationship;

import java.sql.Connection;
import java.util.Map;

public class DBSingletonRelationshipDataFetcher extends DBAbstractSingletonDataFetcher {
    private final Relationship relationship;

    public DBSingletonRelationshipDataFetcher(Relationship relationship, Connection connection) {
        super(connection);
        this.relationship = relationship;
    }

    @Override
    String queryFor(Map<String,Object> source) {
        var substitutor = new StringSubstitutor(key -> {
            if(key.startsWith("string:")) {
                return SQLUtils.asEscapedSqlString(source.get(key.substring("string:".length())).toString());
            } else {
                return source.get(key).toString();
            }
        });
        var joinInstance = substitutor.replace(relationship.join());
        return """
                    SELECT *
                    FROM %s
                    WHERE %s
                """.formatted(relationship.target(), joinInstance);
    }
}
