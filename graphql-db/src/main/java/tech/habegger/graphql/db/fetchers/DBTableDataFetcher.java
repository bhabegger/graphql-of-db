package tech.habegger.graphql.db.fetchers;

import java.sql.Connection;
import java.util.Map;

public class DBTableDataFetcher extends DBAbstractListDataFetcher {
    private final String tableName;

    public DBTableDataFetcher(String tableName, Connection dbConnection) {
        super(dbConnection);
        this.tableName = tableName;
    }

    @Override
    String queryFor(Map<String,Object> source) {
        return """
                   SELECT *
                   FROM %s
               """.formatted(tableName);
    }

}
