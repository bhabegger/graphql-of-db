package tech.habegger.graphql.db.fetchers;

public class SQLUtils {
    public static String asEscapedSqlString(String raw) {
        return "'" + raw.replaceAll("'", "''") + "'";
    }
}
