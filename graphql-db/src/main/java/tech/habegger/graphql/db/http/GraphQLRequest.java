package tech.habegger.graphql.db.http;

import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
public record GraphQLRequest(
        String query,
        Map<String, Object> variables,
        String operationName)
{ }
