package tech.habegger.graphql.db.http;

import com.fasterxml.jackson.databind.ObjectMapper;
import graphql.ExecutionResult;
import org.apache.logging.log4j.core.util.IOUtils;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.io.Content;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Response;
import org.eclipse.jetty.util.Callback;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import tech.habegger.graphql.db.DBGraphQLService;
import tech.habegger.graphql.db.meta.Relationship;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;

import static org.eclipse.jetty.http.HttpHeader.CONTENT_TYPE;

public class GraphQLServlet extends Handler.Abstract {
    private final Logger LOG = LoggerFactory.getLogger(GraphQLServlet.class);
    private final static ObjectMapper MAPPER = new ObjectMapper();
    private final DBGraphQLService graphQLRuntime;

    public GraphQLServlet(Connection dbConnection, Map<String, Map<String, Relationship>> relationships) throws SQLException {
        graphQLRuntime = new DBGraphQLService(dbConnection, relationships);
    }

    @Override
    public boolean handle(Request req, Response response, Callback callback) throws Exception {
        GraphQLRequest request = fromHttpRequest(req);
        LOG.info("{} {}, query={} variables={}", req.getMethod(), req.getContext().getContextPath(), request.query().trim().replaceAll("\\s+"," "), MAPPER.writeValueAsString(request.variables()));

        ExecutionResult result = graphQLRuntime.execute(request.query(), request.variables());

        response.setStatus(HttpStatus.OK_200);
        response.getHeaders().add(CONTENT_TYPE.name(), MimeType.APPLICATION_JSON.withCharset(StandardCharsets.UTF_8));
        Content.Sink.write(response, true, MAPPER.writeValueAsString(result), callback);
        return true;
    }

    private static GraphQLRequest fromHttpRequest(Request req) throws IOException {
        var contentType = req.getHeaders().get(CONTENT_TYPE);
        MimeType mimeType = MimeType.of(contentType);
        Charset charset = StandardCharsets.UTF_8;

        if(mimeType == null) {
            throw new UnsupportedOperationException("Don't know how to handle "+contentType);
        }

        InputStream is = Content.Source.asInputStream(req);

        return switch (mimeType) {
            case APPLICATION_GRAPHQL -> unmarshalGraphQLRequest(is, charset);
            case APPLICATION_JSON -> unmarshalJsonRequest(is, charset);
            default ->
                throw new UnsupportedOperationException("Don't know how to handle "+contentType);
        };
    }

    private static GraphQLRequest unmarshalGraphQLRequest(InputStream is, Charset charset) throws IOException {
        StringWriter writer = new StringWriter();
        IOUtils.copy(new InputStreamReader(is, charset), writer);
        return new GraphQLRequest(writer.toString(), Map.of(), null);
    }

    private static GraphQLRequest unmarshalJsonRequest(InputStream content, Charset charset) throws IOException {
        return MAPPER.readValue(new InputStreamReader(content, charset), GraphQLRequest.class);
    }

}