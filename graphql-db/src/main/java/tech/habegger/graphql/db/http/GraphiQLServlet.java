package tech.habegger.graphql.db.http;

import org.apache.logging.log4j.core.util.IOUtils;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.HttpStatus;
import org.eclipse.jetty.io.Content;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Request;
import org.eclipse.jetty.server.Response;
import org.eclipse.jetty.util.Callback;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;

public class GraphiQLServlet extends Handler.Abstract {
    public final String graphiQLHtml;

    public GraphiQLServlet() {
        try (var is = GraphiQLServlet.class.getResourceAsStream("/graphiql.html")) {
            StringWriter writer = new StringWriter();
            if(is == null) {
                throw new RuntimeException("Unable to fin graphiql.html on resource path");
            }
            IOUtils.copy(new InputStreamReader(is, StandardCharsets.UTF_8), writer);
            this.graphiQLHtml = writer.toString();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean handle(Request request, Response response, Callback callback) {
        response.setStatus(HttpStatus.OK_200);
        response.getHeaders().add(HttpHeader.CONTENT_TYPE, MimeType.TEXT_HTML.withCharset(StandardCharsets.UTF_8));
        Content.Sink.write(response, true, graphiQLHtml, callback);
        return true;
    }
}
