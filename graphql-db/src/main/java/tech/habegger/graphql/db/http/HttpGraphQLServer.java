package tech.habegger.graphql.db.http;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.ContextHandler;
import org.eclipse.jetty.server.handler.ContextHandlerCollection;
import tech.habegger.graphql.db.Config;

public class HttpGraphQLServer {

    public static void main(String[] args) throws Exception {
        Config config = new Config();
        Server server = new Server(config.port());

        // Setup the contexts
        ContextHandlerCollection contextCollection = new ContextHandlerCollection();
        contextCollection.addHandler(new ContextHandler(new GraphiQLServlet(), "/"));
        contextCollection.addHandler(new ContextHandler(new GraphQLServlet(config.connection(), config.relationships()), "/graphql"));
        server.setHandler(contextCollection);

        // Start the HTTP server
        server.start();
        server.join();
    }

}