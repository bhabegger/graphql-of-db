package tech.habegger.graphql.db.http;

import java.nio.charset.Charset;
import java.util.Map;

public enum MimeType {
    APPLICATION_JSON("application/json"),
    APPLICATION_GRAPHQL("application/graphql"),
    TEXT_HTML("text/html");

    static final Map<CharSequence, MimeType> REVERSE = Map.of(
        APPLICATION_JSON.contentType, APPLICATION_JSON,
        APPLICATION_GRAPHQL.contentType, APPLICATION_GRAPHQL
    );

    private final String contentType;

    MimeType(String contentType) {
        this.contentType = contentType;
    }

    public static MimeType of(CharSequence mimeType) {
        if(mimeType == null) {
            return null;
        }
        return REVERSE.get(mimeType);
    }

    public String withCharset(Charset charset) {
        return this.contentType+";"+charset;
    }
}
