package tech.habegger.graphql.db.meta;

public record Relationship(String target, String join, boolean isList) {
}
